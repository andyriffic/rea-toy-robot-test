import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {StoreModule} from '@ngrx/store';
import {robotReducer} from './store/reducers/robot.reducer';
import {reportOutputReducer} from './store/reducers/report-output.reducer';
import { MainComponent } from './components/main/main.component';
import { RobotInputComponent } from './components/robot-input/robot-input.component';
import {FormsModule} from '@angular/forms';
import { RobotOutputComponent } from './components/robot-output/robot-output.component';
import {CommandFactoryService} from './services/command-factory/command-factory.service';
import {TextService} from './services/text/text.service';
import {CommandRunnerService} from './services/command-runner/command-runner.service';
import {PositioningService} from './services/positioning/positioning.service';


@NgModule({
  declarations: [
    MainComponent,
    RobotInputComponent,
    RobotOutputComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    StoreModule.forRoot({robot: robotReducer, reportOutput: reportOutputReducer})
  ],
  providers: [
    CommandRunnerService,
    CommandFactoryService,
    PositioningService,
    TextService
  ],
  bootstrap: [MainComponent]
})
export class AppModule {
}
