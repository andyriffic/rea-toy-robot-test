import {facingDirection} from '../enums/facing-direction.enum';
import {IBoardCoordinate} from './board-coordinate.interface';

export interface IRobot {
  direction: facingDirection;
  position?: IBoardCoordinate;
}

