import {IRobot} from './robot.interface';

export interface IAppState {
  robot: IRobot;
  reportOutput: string[];
}

export const getRobotState = (state: IAppState) => state.robot;
export const getReportOutputState = (state: IAppState) => state.reportOutput;
