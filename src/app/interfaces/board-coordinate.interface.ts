
export interface IBoardCoordinate {
  x: number;
  y: number;
}
