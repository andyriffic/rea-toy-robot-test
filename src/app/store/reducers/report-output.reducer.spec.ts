import {ReportOutputActions, reportOutputReducer} from './report-output.reducer';

describe('Report Output Reducer', () => {

  it('Should return initial state if unknown action', () => {
    const initialState: string[] = [];
    const action = {
      type: 'Not a known action',
      payload: null
    };

    const result = reportOutputReducer(initialState, action);
    expect(result).toBe(initialState);
  });

  it('Should add log entry and return new state instance', () => {
    const initialState: string[] = [];
    const action = {
      type: ReportOutputActions.addEntry,
      payload: 'REPORT_OUTPUT'
    };

    const result = reportOutputReducer(initialState, action);
    expect(result).not.toBe(initialState);
    expect(result.length).toBe(1);
    expect(result[0]).toBe('REPORT_OUTPUT');
  });


  it('Should clear all log entries', () => {
    const initialState: string[] = ['log1', 'log2'];
    const action = {
      type: ReportOutputActions.clear,
      payload: null
    };

    const result = reportOutputReducer(initialState, action);
    expect(result).not.toBe(initialState);
    expect(result.length).toBe(0);
  });

});
