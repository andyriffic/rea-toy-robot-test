import {RobotActions, robotReducer} from './robot.reducer';
import {facingDirection} from '../../enums/facing-direction.enum';
import {IRobot} from '../../interfaces/robot.interface';
import {IBoardCoordinate} from '../../interfaces/board-coordinate.interface';

describe('Robot Reducer', () => {

  it('Should return initial state if unknown action', () => {

    const initialState: IRobot = {direction: facingDirection.NORTH};
    const action = {
      type: 'Not a known action',
      payload: null
    };

    const result = robotReducer(initialState, action);
    expect(result.direction).toBe(facingDirection.NORTH);
    expect(result === initialState).toBe(true);
  });

  it('Should set direction and return different state instance', () => {
    const initialState: IRobot = {direction: facingDirection.NORTH};
    const action = {
      type: RobotActions.setDirection,
      payload: facingDirection.SOUTH
    };

    const result = robotReducer(initialState, action);
    expect(result.direction).toBe(facingDirection.SOUTH);
    expect(result.position).toBe(undefined);
    expect(result !== initialState).toBe(true);
  });

  it('Should set position and return different state instance', () => {
    const initialState: IRobot = {direction: facingDirection.NORTH};
    const coordinate = <IBoardCoordinate>{x: 0, y: 0};
    const action = {
      type: RobotActions.setPosition,
      payload: coordinate
    };

    const result = robotReducer(initialState, action);
    expect(result.direction).toBe(facingDirection.NORTH);
    expect(result.position).toBe(coordinate);
    expect(result.position.x).toBe(coordinate.x);
    expect(result.position.y).toBe(coordinate.y);
    expect(result !== initialState).toBe(true);
  });

  it('Should place robot by setting position and direction and be different robot instance', () => {
    const initialState: IRobot = {direction: facingDirection.NORTH};

    const placeCoordinate = <IBoardCoordinate>{x: 2, y: 2};
    const placeDirection = facingDirection.EAST;
    const action = {
      type: RobotActions.place,
      payload: {
        position: placeCoordinate,
        direction: placeDirection
      }
    };

    const result = robotReducer(initialState, action);
    expect(result.direction).toBe(placeDirection);
    expect(result.position).toBe(placeCoordinate);
    expect(result.position.x).toBe(placeCoordinate.x);
    expect(result.position.y).toBe(placeCoordinate.y);
    expect(result).not.toBe(initialState);
  });

});
