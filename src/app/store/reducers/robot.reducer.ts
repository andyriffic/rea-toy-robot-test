import {facingDirection} from '../../enums/facing-direction.enum';
import {IRobot} from '../../interfaces/robot.interface';

export const RobotActions = {
  setDirection: 'SET_DIRECTION',
  setPosition: 'SET_POSITION',
  place: 'PLACE'
};

export const robotReducer = (state: IRobot = {direction: facingDirection.NORTH}, {type, payload}) => {
  switch (type) {
    case RobotActions.setDirection:
      const newRobotState = Object.assign({}, state, {direction: payload});
      return newRobotState;

    case RobotActions.setPosition:
      const newSetPositionState = Object.assign({}, state, {position: payload});
      return newSetPositionState;

    case RobotActions.place:
      // TODO: this is a combo of setPosition and setDirection (above), could refactor into common code...
      // ...Not too concerned at the moment as this is a pretty light-weight reducer
      const newPlaceState = Object.assign({}, state, {position: payload.position, direction: payload.direction});
      return newPlaceState;

    default:
      return state;
  }
};
