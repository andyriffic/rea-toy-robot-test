
export const ReportOutputActions = {
  addEntry: 'ADD_REPORT_OUTPUT_ENTRY',
  clear: 'CLEAR_REPORT_OUTPUT'
};

export const reportOutputReducer = (state: string[] = [], {type, payload}) => {
  switch (type) {
    case ReportOutputActions.addEntry:
      return [...state, payload];

    case ReportOutputActions.clear:
      return [];

    default:
      return state;
  }
};
