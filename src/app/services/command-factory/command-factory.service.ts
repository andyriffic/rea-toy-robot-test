import { Injectable } from '@angular/core';
import {RobotTurnCommand} from '../../commands/robot-turn.command';
import {turnDirection} from '../../enums/turn-direction.enum';
import {RobotReportCommand} from '../../commands/robot-report.command';
import {RobotMoveCommand} from '../../commands/robot-move.command';
import {facingDirection} from '../../enums/facing-direction.enum';
import {RobotPlaceCommand} from '../../commands/robot-place.command';
import {IAppState} from '../../interfaces/app-state.interface';
import {Store} from '@ngrx/store';
import {PositioningService} from '../positioning/positioning.service';
import {ICommand} from '../../interfaces/command.interface';
import {IPlaceCommandParams} from '../../interfaces/place-command-params.interface';
import {IBoardCoordinate} from '../../interfaces/board-coordinate.interface';

@Injectable()
export class CommandFactoryService {

  constructor(private _positioningService: PositioningService, private _store: Store<IAppState>) { }

  createFromTextLines(cmdLines: string[]): ICommand[] {

    const commands: ICommand[] = [];

    cmdLines.forEach(cmdText => {

      let command: ICommand;

      // TODO: limited commands so decided on simple if statements here to determine what the command is
      if (cmdText === 'LEFT') {
        command = new RobotTurnCommand(this._store, turnDirection.LEFT, this._positioningService);

      } else if (cmdText === 'RIGHT') {
        command = new RobotTurnCommand(this._store, turnDirection.RIGHT, this._positioningService);

      } else if (cmdText === 'MOVE') {
        command = new RobotMoveCommand(this._store, this._positioningService);

      } else if (cmdText === 'REPORT') {
        command = new RobotReportCommand(this._store);

      } else if (/^PLACE \d,\d,(?:NORTH|EAST|SOUTH|WEST)$/.test(cmdText)) {
        const placeParams = this.getPlaceCommandParams(cmdText);
        command = new RobotPlaceCommand(this._store, placeParams.position, placeParams.direction, this._positioningService);

      } else {
        console.warn(`Text "${cmdText}" does not match any known command`);
      }

      if (command) {
        commands.push(command);
      }

    });

    return commands;
  }

  private getPlaceCommandParams(correctlyFormattedPlaceCommand: string): IPlaceCommandParams {
    // TODO: could this be more elegant? Fine for now and has test coverage on CommandFactory so can refactor later

    // Assuming input format has already been validated
    const ARGS_POSITION = 1;
    const X_POS_ARGS_POSITION = 0;
    const Y_POS_ARGS_POSITION = 1;
    const DIRECTION_ARGS_POSITION = 2;
    const RADIX = 10;

    const args = correctlyFormattedPlaceCommand.split(' ')[ARGS_POSITION].split(','); // TODO: ugly :(
    const xPos = parseInt(args[X_POS_ARGS_POSITION], RADIX);
    const yPos = parseInt(args[Y_POS_ARGS_POSITION], RADIX);
    const position = <IBoardCoordinate>{x: xPos, y: yPos}; // Note: don't care if invalid position at this point
    const direction: facingDirection = facingDirection[args[DIRECTION_ARGS_POSITION]];

    return <IPlaceCommandParams>{ position: position, direction: direction };
  }

}
