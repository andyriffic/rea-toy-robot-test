import {TestBed, inject} from '@angular/core/testing';
import {CommandFactoryService} from './command-factory.service';
import {RobotTurnCommand} from '../../commands/robot-turn.command';
import {turnDirection} from '../../enums/turn-direction.enum';
import {RobotMoveCommand} from '../../commands/robot-move.command';
import {RobotReportCommand} from '../../commands/robot-report.command';
import {RobotPlaceCommand} from '../../commands/robot-place.command';
import {facingDirection} from '../../enums/facing-direction.enum';
import {PositioningService} from '../positioning/positioning.service';
import {StoreModule} from '@ngrx/store';
import {robotReducer} from '../../store/reducers/robot.reducer';

describe('CommandFactoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({robot: robotReducer})
      ],
      providers: [
        CommandFactoryService,
        PositioningService
      ]
    });
  });

  it('should be created', inject([CommandFactoryService], (service: CommandFactoryService) => {
    expect(service).toBeTruthy();
  }));

  it('should return no commands if no text', inject([CommandFactoryService], (service: CommandFactoryService) => {
    const commands = service.createFromTextLines(['']);

    expect(commands.length).toBe(0);
  }));

  it('should return no commands if rubbish text', inject([CommandFactoryService], (service: CommandFactoryService) => {
    const commands = service.createFromTextLines(['blah']);
    expect(commands.length).toBe(0);
  }));


  it('should create a TURN command if LEFT', inject([CommandFactoryService], (service: CommandFactoryService) => {
    const commands = service.createFromTextLines(['LEFT']);
    expect(commands.length).toBe(1);
    expect(commands[0] instanceof RobotTurnCommand).toBeTruthy();

    const turnCommand = commands[0] as RobotTurnCommand;
    expect(turnCommand.turnDirection).toBe(turnDirection.LEFT);
  }));


  it('should create a TURN command if "RIGHT"', inject([CommandFactoryService], (service: CommandFactoryService) => {
    const commands = service.createFromTextLines(['RIGHT']);
    expect(commands.length).toBe(1);
    expect(commands[0] instanceof RobotTurnCommand).toBeTruthy();

    const turnCommand = commands[0] as RobotTurnCommand;
    expect(turnCommand.turnDirection).toBe(turnDirection.RIGHT);
  }));

  it('should create a MOVE command if "MOVE"', inject([CommandFactoryService], (service: CommandFactoryService) => {
    const commands = service.createFromTextLines(['MOVE']);
    expect(commands.length).toBe(1);
    expect(commands[0] instanceof RobotMoveCommand).toBeTruthy();
  }));

  it('should create a REPORT command if "REPORT"', inject([CommandFactoryService], (service: CommandFactoryService) => {
    const commands = service.createFromTextLines(['REPORT']);
    expect(commands.length).toBe(1);
    expect(commands[0] instanceof RobotReportCommand).toBeTruthy();
  }));

  it('should create a NORTH PLACE command', inject([CommandFactoryService], (service: CommandFactoryService) => {
    const commands = service.createFromTextLines(['PLACE 1,2,NORTH']);
    expect(commands.length).toBe(1);
    expect(commands[0] instanceof RobotPlaceCommand).toBeTruthy();

    const placeCommand = commands[0] as RobotPlaceCommand;
    expect(placeCommand.position.x).toBe(1);
    expect(placeCommand.position.y).toBe(2);
    expect(placeCommand.direction).toBe(facingDirection.NORTH);
  }));

  it('should create a EAST PLACE command', inject([CommandFactoryService], (service: CommandFactoryService) => {
    const commands = service.createFromTextLines(['PLACE 1,2,EAST']);
    expect(commands.length).toBe(1);
    expect(commands[0] instanceof RobotPlaceCommand).toBeTruthy();

    const placeCommand = commands[0] as RobotPlaceCommand;
    expect(placeCommand.position.x).toBe(1);
    expect(placeCommand.position.y).toBe(2);
    expect(placeCommand.direction).toBe(facingDirection.EAST);
  }));

  it('should create a SOUTH PLACE command', inject([CommandFactoryService], (service: CommandFactoryService) => {
    const commands = service.createFromTextLines(['PLACE 1,2,SOUTH']);
    expect(commands.length).toBe(1);
    expect(commands[0] instanceof RobotPlaceCommand).toBeTruthy();

    const placeCommand = commands[0] as RobotPlaceCommand;
    expect(placeCommand.position.x).toBe(1);
    expect(placeCommand.position.y).toBe(2);
    expect(placeCommand.direction).toBe(facingDirection.SOUTH);
  }));

  it('should create a WEST PLACE command', inject([CommandFactoryService], (service: CommandFactoryService) => {
    const commands = service.createFromTextLines(['PLACE 1,2,WEST']);
    expect(commands.length).toBe(1);
    expect(commands[0] instanceof RobotPlaceCommand).toBeTruthy();

    const placeCommand = commands[0] as RobotPlaceCommand;
    expect(placeCommand.position.x).toBe(1);
    expect(placeCommand.position.y).toBe(2);
    expect(placeCommand.direction).toBe(facingDirection.WEST);
  }));

  it('should not create PLACE command from rubbish text', inject([CommandFactoryService], (service: CommandFactoryService) => {
    const commands = service.createFromTextLines([
      'PLACE 1,2,WESTx',
      'PLACE1,2,WEST',
      'PLACE x,2,NORTH',
      'PLACE 3,y,SOUTH',
      'PLACE 20,3,NORTH',
      'PLACE 4, 2,Blah'
    ]);
    expect(commands.length).toBe(0);
  }));

  it('should create multiple commands in correct order', inject([CommandFactoryService], (service: CommandFactoryService) => {
    const commands = service.createFromTextLines([
      'PLACE 1,2,WEST',
      'MOVE',
      'RIGHT',
      'LEFT',
      'REPORT'
    ]);
    expect(commands.length).toBe(5);
    expect(commands[0] instanceof RobotPlaceCommand).toBeTruthy();
    expect(commands[1] instanceof RobotMoveCommand).toBeTruthy();
    expect(commands[2] instanceof RobotTurnCommand).toBeTruthy();
    expect(commands[3] instanceof RobotTurnCommand).toBeTruthy();
    expect(commands[4] instanceof RobotReportCommand).toBeTruthy();
  }));

});
