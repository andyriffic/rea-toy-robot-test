import { TestBed, inject } from '@angular/core/testing';
import { CommandRunnerService } from './command-runner.service';
import {CommandFactoryService} from '../command-factory/command-factory.service';
import {PositioningService} from '../positioning/positioning.service';
import {TextService} from '../text/text.service';
import {reportOutputReducer} from '../../store/reducers/report-output.reducer';
import {Store, StoreModule} from '@ngrx/store';
import {robotReducer} from '../../store/reducers/robot.reducer';
import {IAppState} from '../../interfaces/app-state.interface';

describe('CommandRunnerService', () => {
  let _service: CommandRunnerService;
  let _latestState: IAppState;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({robot: robotReducer, reportOutput: reportOutputReducer})
      ],
      providers: [
        CommandRunnerService,
        CommandFactoryService,
        PositioningService,
        TextService
      ]
    });
  });

  beforeEach(inject([Store, CommandRunnerService], (store: Store<IAppState>, service: CommandRunnerService) => {
    _service = service;
    store.subscribe(state => {
      _latestState = state;
    });
  }));

  it('should be created', () => {
    expect(_service).toBeTruthy();
  });

  /// ***************************************************
  /// SCENARIO TEST EXPECTED OUTPUT FROM EXAMPLES IN SPEC
  /// ***************************************************
  it('should parse commands and run (Example A)', () => {
    const commandInput = 'PLACE 0,0,NORTH\n' +
      'MOVE\n' +
      'REPORT';

    _service.parseAndExecuteRobotCommands(commandInput);

    expect(_latestState.reportOutput[0]).toBe('0,1,NORTH');
  });


  it('should parse commands and run (Example B)', () => {
    const commandInput = 'PLACE 0,0,NORTH\n' +
      'LEFT\n' +
      'REPORT';

    _service.parseAndExecuteRobotCommands(commandInput);

    expect(_latestState.reportOutput[0]).toBe('0,0,WEST');
  });

  it('should parse commands and run (Example C)', () => {
    const commandInput = 'PLACE 1,2,EAST\n' +
      'MOVE\n' +
      'MOVE\n' +
      'LEFT\n' +
      'MOVE\n' +
      'REPORT';

    _service.parseAndExecuteRobotCommands(commandInput);

    expect(_latestState.reportOutput[0]).toBe('3,3,NORTH');
  });

});
