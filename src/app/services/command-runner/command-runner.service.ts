import {Injectable} from '@angular/core';
import {CommandFactoryService} from '../command-factory/command-factory.service';
import {TextService} from '../text/text.service';

@Injectable()
export class CommandRunnerService {

  constructor(private _commandFactory: CommandFactoryService, private _textService: TextService) {
  }

  parseAndExecuteRobotCommands(cmdLines: string): void {
    const cmdLineArray = this._textService.toFormattedCommandTextLines(cmdLines);
    const robotCommands = this._commandFactory.createFromTextLines(cmdLineArray);

    robotCommands.forEach((cmd) => {
      cmd.execute();
    });
  }
}
