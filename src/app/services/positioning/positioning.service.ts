import { Injectable } from '@angular/core';
import {turnDirection} from '../../enums/turn-direction.enum';
import {facingDirection} from '../../enums/facing-direction.enum';
import {IBoardCoordinate} from '../../interfaces/board-coordinate.interface';

@Injectable()
export class PositioningService {

  private _boardMaxCoordinateX = 4; // Zero based index so actually 5 cells wide
  private _boardMaxCoordinateY = 4; // Zero based index so actually 5 cells wide

  private _directionOrder: facingDirection[] = [
    facingDirection.NORTH,
    facingDirection.EAST,
    facingDirection.SOUTH,
    facingDirection.WEST
  ];

  constructor() { }

  isValidBoardPosition(coordinate: IBoardCoordinate): boolean {
    if (coordinate.x < 0 || coordinate.x > this._boardMaxCoordinateX) {
      return false;
    }

    if (coordinate.y < 0 || coordinate.y > this._boardMaxCoordinateY) {
      return false;
    }

    return true;
  }

  moveFromCoordinateInDirection(coordinate: IBoardCoordinate, direction: facingDirection): IBoardCoordinate {
    let destinationCoordinate: IBoardCoordinate;
    switch (direction) {
      case facingDirection.NORTH:
        destinationCoordinate = {x: coordinate.x, y: coordinate.y + 1};
        break;

      case facingDirection.EAST:
        destinationCoordinate = {x: coordinate.x + 1, y: coordinate.y};
        break;

      case facingDirection.SOUTH:
        destinationCoordinate = {x: coordinate.x, y: coordinate.y - 1};
        break;

      case facingDirection.WEST:
        destinationCoordinate = {x: coordinate.x - 1, y: coordinate.y};
        break;

      default:
        throw new Error(`Direction of '${direction}' was not calculated`);
    }

    return destinationCoordinate; // Returning coordinate even tho it may be outside board
  }

  getDirectionAfterTurning(direction: turnDirection, currentFacing: facingDirection): facingDirection {

    // Given we have a direction order, just find current direction in it then move +/- to next index
    const turnOffset = direction === turnDirection.RIGHT ? 1 : -1;
    const currentDirectionOrderIndex = this._directionOrder.indexOf(currentFacing);

    let nextDirectionOrderIndex = currentDirectionOrderIndex + turnOffset;

    // Account for wrapping around index edges
    if (nextDirectionOrderIndex === -1) {
      nextDirectionOrderIndex = this._directionOrder.length - 1;
    } else if (nextDirectionOrderIndex === this._directionOrder.length) {
      nextDirectionOrderIndex = 0;
    }

    return this._directionOrder[nextDirectionOrderIndex];
  }

}
