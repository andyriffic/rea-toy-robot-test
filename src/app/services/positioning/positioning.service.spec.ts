import {TestBed, inject} from '@angular/core/testing';
import {PositioningService} from './positioning.service';
import {turnDirection} from '../../enums/turn-direction.enum';
import {facingDirection} from '../../enums/facing-direction.enum';
import {IBoardCoordinate} from '../../interfaces/board-coordinate.interface';
import {generateAllValidBoardCoordinate} from '../../specs/spec-helpers.spec';

describe('PositioningService', () => {

  let _service: PositioningService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PositioningService]
    });
  });

  beforeEach(inject([PositioningService], (service: PositioningService) => {
    _service = service;
  }));

  it('should be created', () => {
    expect(_service).toBeTruthy();
  });

  it('should turn left correctly', () => {
    let turnResult = _service.getDirectionAfterTurning(turnDirection.LEFT, facingDirection.NORTH);
    expect(turnResult).toBe(facingDirection.WEST);

    turnResult = _service.getDirectionAfterTurning(turnDirection.LEFT, facingDirection.EAST);
    expect(turnResult).toBe(facingDirection.NORTH);

    turnResult = _service.getDirectionAfterTurning(turnDirection.LEFT, facingDirection.SOUTH);
    expect(turnResult).toBe(facingDirection.EAST);

    turnResult = _service.getDirectionAfterTurning(turnDirection.LEFT, facingDirection.WEST);
    expect(turnResult).toBe(facingDirection.SOUTH);
  });


  it('should move North correctly', () => {
    const originCoordinate = <IBoardCoordinate>{x: 2, y: 2};
    const destinationCoordinate = _service.moveFromCoordinateInDirection(originCoordinate, facingDirection.NORTH);
    expect(destinationCoordinate.x).toBe(2);
    expect(destinationCoordinate.y).toBe(3);
  });

  it('should move East correctly', () => {
    const originCoordinate = <IBoardCoordinate>{x: 2, y: 2};
    const destinationCoordinate = _service.moveFromCoordinateInDirection(originCoordinate, facingDirection.EAST);
    expect(destinationCoordinate.x).toBe(3);
    expect(destinationCoordinate.y).toBe(2);
  });

  it('should move South correctly', () => {
    const originCoordinate = <IBoardCoordinate>{x: 2, y: 2};
    const destinationCoordinate = _service.moveFromCoordinateInDirection(originCoordinate, facingDirection.SOUTH);
    expect(destinationCoordinate.x).toBe(2);
    expect(destinationCoordinate.y).toBe(1);
  });

  it('should move West correctly', () => {
    const originCoordinate = <IBoardCoordinate>{x: 2, y: 2};
    const destinationCoordinate = _service.moveFromCoordinateInDirection(originCoordinate, facingDirection.WEST);
    expect(destinationCoordinate.x).toBe(1);
    expect(destinationCoordinate.y).toBe(2);
  });

  it('should turn right correctly', () => {
    let turnResult = _service.getDirectionAfterTurning(turnDirection.RIGHT, facingDirection.NORTH);
    expect(turnResult).toBe(facingDirection.EAST);

    turnResult = _service.getDirectionAfterTurning(turnDirection.RIGHT, facingDirection.EAST);
    expect(turnResult).toBe(facingDirection.SOUTH);

    turnResult = _service.getDirectionAfterTurning(turnDirection.RIGHT, facingDirection.SOUTH);
    expect(turnResult).toBe(facingDirection.WEST);

    turnResult = _service.getDirectionAfterTurning(turnDirection.RIGHT, facingDirection.WEST);
    expect(turnResult).toBe(facingDirection.NORTH);
  });

  it('should return all board coordinates as valid', () => {
    // Might as well test every board cell since a square grid
    const allValidCoordinates = generateAllValidBoardCoordinate();
    allValidCoordinates.forEach(coordinate => {
      const isValid = _service.isValidBoardPosition(coordinate);
      expect(isValid).toBe(true, `Coordinate (${coordinate.x},${coordinate.y}) returned invalid when it should be valid`);
    });
  });

  it('should detect cells outside board as invalid', () => {
    expectInvalidBoardCoordinate(-1, -1); // Boundary lower
    expectInvalidBoardCoordinate(5, 5); // Boundary higher
    expectInvalidBoardCoordinate(1, 10); // X valid, Y invalid
    expectInvalidBoardCoordinate(60, 2); // X invalid, Y valid
  });

  function expectInvalidBoardCoordinate(x: number, y: number) {
    const coordinate = <IBoardCoordinate>{x, y};

    expect(_service.isValidBoardPosition(coordinate))
      .toBe(false, `Coordinate (${x},${y}) returned valid when it should be invalid`);
  }

});
