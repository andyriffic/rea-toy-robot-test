import {Injectable} from '@angular/core';
import {GlobalConstants} from '../../constants/global.constants';

@Injectable()
export class TextService {

  constructor() {
  }

  toFormattedCommandTextLines(text: string): string[] {
    if (!text) {
      return [];
    }

    return text.split(GlobalConstants.NEW_LINE)
      .map((line) => line.trim())
      .map((line) => line.toUpperCase());
  }
}
