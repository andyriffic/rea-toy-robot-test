import {TestBed, inject} from '@angular/core/testing';
import {TextService} from './text.service';
import {GlobalConstants} from '../../constants/global.constants';


describe('TextService', () => {
  const _newLine = GlobalConstants.NEW_LINE;

  let _service: TextService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TextService]
    });
  });

  beforeEach(inject([TextService], (service: TextService) => {
    _service = service;
  }));

  it('should be created', () => {
    expect(_service).toBeTruthy();
  });

  it('should return empty array if no text supplied', () => {
    const textLines = _service.toFormattedCommandTextLines(undefined);

    expect(textLines.length).toBe(0);
  });

  it('should return empty array if blank text', () => {
    const textLines = _service.toFormattedCommandTextLines('');

    expect(textLines.length).toBe(0);
  });

  it('should return multiple text lines, converted to uppercase and trimmed', () => {
    const textLines = _service.toFormattedCommandTextLines(`Hello  ${_newLine} how${_newLine} are ${_newLine}you`);

    expect(textLines.length).toBe(4);
    expect(textLines[0]).toBe('HELLO');
    expect(textLines[1]).toBe('HOW');
    expect(textLines[2]).toBe('ARE');
    expect(textLines[3]).toBe('YOU');
  });
});
