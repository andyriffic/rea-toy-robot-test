import {IAppState} from '../interfaces/app-state.interface';
import {Store} from '@ngrx/store';
import {facingDirection} from '../enums/facing-direction.enum';
import {ReportOutputActions} from '../store/reducers/report-output.reducer';
import {BaseRobotCommand} from './base-robot-command';

export class RobotReportCommand extends BaseRobotCommand {

  constructor(protected _store: Store<IAppState>) {
    super(_store);
  }

  execute(): void {
    const positionText = this._robotState.position
      ? `${this._robotState.position.x},${this._robotState.position.y}`
      : 'LIMBO';

    const directionText = facingDirection[this._robotState.direction];
    const reportText = `${positionText},${directionText}`;

    this._store.dispatch({type: ReportOutputActions.addEntry, payload: reportText});
  }

}
