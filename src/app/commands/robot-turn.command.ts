import {turnDirection} from '../enums/turn-direction.enum';
import {IAppState} from '../interfaces/app-state.interface';
import {Store} from '@ngrx/store';
import {PositioningService} from '../services/positioning/positioning.service';
import {RobotActions} from '../store/reducers/robot.reducer';
import {BaseRobotCommand} from './base-robot-command';

export class RobotTurnCommand extends BaseRobotCommand {

  public get turnDirection(): turnDirection {
    return this._turnDirection;
  }

  constructor(protected _store: Store<IAppState>,
              private _turnDirection: turnDirection,
              private _positioningService: PositioningService) {

    super(_store);
  }

  execute(): void {
    const nextDirection = this._positioningService.getDirectionAfterTurning(
      this._turnDirection,
      this._robotState.direction
    );

    this._store.dispatch({type: RobotActions.setDirection, payload: nextDirection});
  }

}
