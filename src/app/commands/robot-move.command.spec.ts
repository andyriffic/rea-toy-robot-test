import {Store, StoreModule} from '@ngrx/store';
import {IAppState} from '../interfaces/app-state.interface';
import {RobotActions, robotReducer} from '../store/reducers/robot.reducer';
import {inject, TestBed} from '@angular/core/testing';
import {PositioningService} from '../services/positioning/positioning.service';
import {facingDirection} from '../enums/facing-direction.enum';
import {RobotMoveCommand} from './robot-move.command';
import {IBoardCoordinate} from '../interfaces/board-coordinate.interface';
import {IRobot} from '../interfaces/robot.interface';

describe('Robot Move Command', () => {

  let _store: Store<IAppState>;
  let _positioningService: PositioningService;
  let _latestRobotState: IRobot;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({robot: robotReducer})
      ],
      providers: [
        PositioningService
      ]
    });
  });


  beforeEach(inject([Store, PositioningService], (store: Store<IAppState>, positioningService) => {
    _store = store;
    _positioningService = positioningService;
    _store.subscribe(state => {
      _latestRobotState = state.robot;
    });
  }));

  it('Should not move if robot not placed', () => {
    // Robot not placed by default
    const command = new RobotMoveCommand(_store, _positioningService);
    command.execute();

    expect(_latestRobotState.direction).toBe(facingDirection.NORTH);
    expect(_latestRobotState.position).toBe(undefined);
  });

  it('Should move to new position if valid', () => {
    const currentCoordinate = <IBoardCoordinate>{x: 2, y: 2};
    _store.dispatch({type: RobotActions.setDirection, payload: facingDirection.EAST});
    _store.dispatch({type: RobotActions.setPosition, payload: currentCoordinate});

    const command = new RobotMoveCommand(_store, _positioningService);
    command.execute();

    expect(_latestRobotState.direction).toBe(facingDirection.EAST);
    expect(_latestRobotState.position).toBeTruthy();
    expect(_latestRobotState.position !== currentCoordinate).toBe(true);
    expect(_latestRobotState.position.x).toBe(3);
    expect(_latestRobotState.position.y).toBe(2);
  });

  it('Should not update position if invalid destination', () => {
    // Try moving west on the left-most board column
    const currentCoordinate = <IBoardCoordinate>{x: 0, y: 0};
    _store.dispatch({type: RobotActions.setDirection, payload: facingDirection.WEST});
    _store.dispatch({type: RobotActions.setPosition, payload: currentCoordinate});

    const command = new RobotMoveCommand(_store, _positioningService);
    command.execute();

    expect(_latestRobotState.direction).toBe(facingDirection.WEST);
    expect(_latestRobotState.position).toBe(currentCoordinate);
    expect(_latestRobotState.position.x).toBe(currentCoordinate.x);
    expect(_latestRobotState.position.y).toBe(currentCoordinate.y);
  });
});
