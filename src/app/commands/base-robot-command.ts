import {IRobot} from '../interfaces/robot.interface';
import {Store} from '@ngrx/store';
import {getRobotState, IAppState} from '../interfaces/app-state.interface';
import {ICommand} from '../interfaces/command.interface';

export abstract class BaseRobotCommand implements ICommand {

  protected _robotState: IRobot;

  constructor(protected _store: Store<IAppState>) {
    _store.select(getRobotState).subscribe(robotState => {
      this._robotState = robotState;
    });
  }

  execute(): void {
    throw new Error('Method not implemented.');
  }

}
