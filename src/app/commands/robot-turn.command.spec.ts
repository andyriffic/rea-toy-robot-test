import {RobotTurnCommand} from './robot-turn.command';
import {turnDirection} from '../enums/turn-direction.enum';
import {Store, StoreModule} from '@ngrx/store';
import {IAppState} from '../interfaces/app-state.interface';
import {RobotActions, robotReducer} from '../store/reducers/robot.reducer';
import {inject, TestBed} from '@angular/core/testing';
import {PositioningService} from '../services/positioning/positioning.service';
import {facingDirection} from '../enums/facing-direction.enum';
import {IRobot} from '../interfaces/robot.interface';

describe('Robot Turn Command', () => {

  let _store: Store<IAppState>;
  let _positioningService: PositioningService;
  let _latestRobotState: IRobot;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({robot: robotReducer})
      ],
      providers: [
        PositioningService
      ]
    });
  });


  beforeEach(inject([Store, PositioningService], (store: Store<IAppState>, positioningService) => {
    _store = store;
    _positioningService = positioningService;
    _store.subscribe(state => {
      _latestRobotState = state.robot;
    });
  }));

  it('Should update Robot State when turning right', () => {
    _store.dispatch({type: RobotActions.setDirection, payload: facingDirection.NORTH});
    const command = new RobotTurnCommand(_store, turnDirection.RIGHT, _positioningService);
    command.execute();

    expect(_latestRobotState.direction).toBe(facingDirection.EAST);
  });

  it('Should update Robot State when turning left', () => {
    _store.dispatch({type: RobotActions.setDirection, payload: facingDirection.SOUTH});
    const command = new RobotTurnCommand(_store, turnDirection.LEFT, _positioningService);
    command.execute();

    expect(_latestRobotState.direction).toBe(facingDirection.EAST);
  });
});
