import {Store, StoreModule} from '@ngrx/store';
import {IAppState} from '../interfaces/app-state.interface';
import {RobotActions, robotReducer} from '../store/reducers/robot.reducer';
import {inject, TestBed} from '@angular/core/testing';
import {PositioningService} from '../services/positioning/positioning.service';
import {RobotReportCommand} from './robot-report.command';
import {ReportOutputActions, reportOutputReducer} from '../store/reducers/report-output.reducer';
import {facingDirection} from '../enums/facing-direction.enum';
import {IRobot} from '../interfaces/robot.interface';
import {IBoardCoordinate} from '../interfaces/board-coordinate.interface';
import {generateAllValidBoardCoordinate} from '../specs/spec-helpers.spec';
import {forEach} from '@angular/router/src/utils/collection';

describe('Robot Report Command', () => {

  let _store: Store<IAppState>;
  let _positioningService: PositioningService;
  let _latestRobotState: IRobot;
  let _latestReportOutput: string[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({robot: robotReducer, reportOutput: reportOutputReducer})
      ],
      providers: [
        PositioningService
      ]
    });
  });

  beforeEach(inject([Store, PositioningService], (store: Store<IAppState>, positioningService) => {
    _store = store;
    _positioningService = positioningService;
    _store.subscribe(state => {
      _latestRobotState = state.robot;
      _latestReportOutput = state.reportOutput;
    });
  }));

  it('Should report on Robot initial state (not placed)', () => {
    const command = new RobotReportCommand(_store);
    command.execute();

    expect(_latestReportOutput[0]).toBe('LIMBO,NORTH');
  });

  it('Should report valid robot placement and direction', () => {
    const position = <IBoardCoordinate>{x: 3, y: 2};
    const direction = facingDirection.WEST;
    _store.dispatch({type: RobotActions.place, payload: {position: position, direction: direction}});

    const command = new RobotReportCommand(_store);
    command.execute();

    expect(_latestReportOutput[0]).toBe('3,2,WEST');
  });

  it('Should report valid placement for all board cells and directions', () => {

    // Given limited valid positions, we can test every board cell and direction for expected report output
    const allValidCoordinates = generateAllValidBoardCoordinate();
    const directions = [facingDirection.NORTH, facingDirection.EAST, facingDirection.SOUTH, facingDirection.WEST];
    const command = new RobotReportCommand(_store);

    allValidCoordinates.forEach(coordinate => {
      directions.forEach((direction) => {
        _store.dispatch({type: RobotActions.place, payload: {position: coordinate, direction: direction}});
        command.execute();
        const expectedLogOutput = `${coordinate.x},${coordinate.y},${facingDirection[direction]}`;
        expect(_latestReportOutput[_latestReportOutput.length - 1]).toBe(expectedLogOutput);
      });
    });
  });

});
