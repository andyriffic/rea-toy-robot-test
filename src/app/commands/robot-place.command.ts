import {facingDirection} from '../enums/facing-direction.enum';
import {IAppState} from '../interfaces/app-state.interface';
import {Store} from '@ngrx/store';
import {PositioningService} from '../services/positioning/positioning.service';
import {RobotActions} from '../store/reducers/robot.reducer';
import {IBoardCoordinate} from '../interfaces/board-coordinate.interface';
import {BaseRobotCommand} from './base-robot-command';

export class RobotPlaceCommand extends BaseRobotCommand {

  get position(): IBoardCoordinate {
    return this._position;
  }

  get direction(): facingDirection {
    return this._direction;
  }

  constructor(protected _store: Store<IAppState>,
              private _position: IBoardCoordinate,
              private _direction: facingDirection,
              private _positioningService: PositioningService) {

    super(_store);
  }

  execute(): void {
    const validPlacement = this._positioningService.isValidBoardPosition(this._position);

    if (!validPlacement) {
      // Ignore place command if invalid position
      console.warn(`Robot not placed, position (${this._position.x},${this._position.y}) is invalid`);
      return;
    }

    this._store.dispatch({
      type: RobotActions.place,
      payload: {position: this._position, direction: this._direction}
    });
  }

}
