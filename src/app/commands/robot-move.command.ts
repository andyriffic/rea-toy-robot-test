import {IAppState} from '../interfaces/app-state.interface';
import {Store} from '@ngrx/store';
import {PositioningService} from '../services/positioning/positioning.service';
import {RobotActions} from '../store/reducers/robot.reducer';
import {BaseRobotCommand} from './base-robot-command';
import {facingDirection} from '../enums/facing-direction.enum';

export class RobotMoveCommand extends BaseRobotCommand {

  constructor(protected _store: Store<IAppState>, private _positioningService: PositioningService) {
    super(_store);
  }

  execute(): void {
    if (!this._robotState.position) {
      console.warn('Movement ignored since robot not placed on board yet');
      return; // Ignore movement if robot not placed on board
    }

    const destinationCoordinate = this._positioningService.moveFromCoordinateInDirection(
      this._robotState.position,
      this._robotState.direction
    );

    if (!this._positioningService.isValidBoardPosition(destinationCoordinate)) {
      console.warn(`Cannot move Robot ${facingDirection[this._robotState.direction]} ` +
        `from position (${this._robotState.position.x}, ${this._robotState.position.y})`);
      return;
    }

    this._store.dispatch({type: RobotActions.setPosition, payload: destinationCoordinate});
  }

}
