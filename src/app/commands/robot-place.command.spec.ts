import {Store, StoreModule} from '@ngrx/store';
import {IAppState} from '../interfaces/app-state.interface';
import {RobotActions, robotReducer} from '../store/reducers/robot.reducer';
import {inject, TestBed} from '@angular/core/testing';
import {PositioningService} from '../services/positioning/positioning.service';
import {facingDirection} from '../enums/facing-direction.enum';
import {RobotPlaceCommand} from './robot-place.command';
import {IBoardCoordinate} from '../interfaces/board-coordinate.interface';
import {IRobot} from '../interfaces/robot.interface';

describe('Robot Place Command', () => {

  let _store: Store<IAppState>;
  let _positioningService: PositioningService;
  let _latestRobotState: IRobot;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({robot: robotReducer})
      ],
      providers: [
        PositioningService
      ]
    });
  });

  beforeEach(inject([Store, PositioningService], (store: Store<IAppState>, positioningService) => {
    _store = store;
    _positioningService = positioningService;
    _store.subscribe(state => {
      _latestRobotState = state.robot;
    });
  }));

  it('Should update robot position and direction if valid placement', () => {
    // Robot not placed by default
    const placeCoordinate = <IBoardCoordinate>{x: 2, y: 2};
    const placeDirection = facingDirection.SOUTH;
    const command = new RobotPlaceCommand(_store, placeCoordinate, placeDirection, _positioningService);
    command.execute();

    expect(_latestRobotState.direction).toBe(placeDirection);
    expect(_latestRobotState.position).toBe(placeCoordinate);
  });


  it('Should not update robot position or direction if invalid placement (when robot not placed yet)', () => {
    // Robot not placed by default
    const initialDirection = facingDirection.NORTH;
    _store.dispatch({type: RobotActions.setDirection, payload: initialDirection});

    const placeCoordinate = <IBoardCoordinate>{x: -1, y: -1};
    const placeDirection = facingDirection.SOUTH;
    const command = new RobotPlaceCommand(_store, placeCoordinate, placeDirection, _positioningService);
    command.execute();

    expect(_latestRobotState.direction).toBe(initialDirection);
    expect(_latestRobotState.position).toBe(undefined);
  });

  it('Should not update robot position or direction if invalid placement (when robot already placed)', () => {
    // Robot not placed by default
    const initialPosition = <IBoardCoordinate>{x: 2, y: 2};
    const initialDirection = facingDirection.NORTH;
    _store.dispatch({type: RobotActions.place, payload: {position: initialPosition, direction: initialDirection}});

    const placeCoordinate = <IBoardCoordinate>{x: -1, y: -1};
    const placeDirection = facingDirection.SOUTH;
    const command = new RobotPlaceCommand(_store, placeCoordinate, placeDirection, _positioningService);
    command.execute();

    expect(_latestRobotState.direction).toBe(initialDirection);
    expect(_latestRobotState.position).toBe(initialPosition);
  });

});
