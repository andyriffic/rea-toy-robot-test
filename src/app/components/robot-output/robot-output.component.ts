import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-robot-output',
  templateUrl: './robot-output.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RobotOutputComponent implements OnInit {

  @Input() outputLines: string[];

  constructor() { }

  ngOnInit() {
  }

}
