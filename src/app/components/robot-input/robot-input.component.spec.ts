import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RobotInputComponent } from './robot-input.component';
import {FormsModule} from '@angular/forms';

describe('RobotInputComponent', () => {
  let component: RobotInputComponent;
  let fixture: ComponentFixture<RobotInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ RobotInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RobotInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
