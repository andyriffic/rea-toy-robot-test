import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-robot-input',
  templateUrl: './robot-input.component.html',
})
export class RobotInputComponent implements OnInit {

  @Output() onRobotCommandsEntered = new EventEmitter<string>();
  robotCommands: string;

  constructor() { }

  ngOnInit() {
  }

  submitCommands(event: Event) {
    event.preventDefault();
    this.onRobotCommandsEntered.emit(this.robotCommands);
  }
}
