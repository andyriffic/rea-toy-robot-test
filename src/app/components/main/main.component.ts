import { Component, OnInit } from '@angular/core';
import {CommandRunnerService} from '../../services/command-runner/command-runner.service';
import {getReportOutputState, IAppState} from '../../interfaces/app-state.interface';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {ReportOutputActions} from '../../store/reducers/report-output.reducer';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  robotOutput: Observable<string[]>;

  constructor(private _store: Store<IAppState>, private _commandRunner: CommandRunnerService) {
    this.robotOutput = _store.select(getReportOutputState);
  }

  ngOnInit() {
  }

  onCommandsEntered(commandText: string): void {
    // TODO: currently not resetting bot state, so running different batch of commands will just continue from existing state
    this._store.dispatch({type: ReportOutputActions.clear, payload: null}); // Clear the log tho
    this._commandRunner.parseAndExecuteRobotCommands(commandText);
  }

}
