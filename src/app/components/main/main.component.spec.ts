import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainComponent } from './main.component';
import {RobotInputComponent} from '../robot-input/robot-input.component';
import {RobotOutputComponent} from '../robot-output/robot-output.component';
import {FormsModule} from '@angular/forms';
import {reportOutputReducer} from '../../store/reducers/report-output.reducer';
import {StoreModule} from '@ngrx/store';
import {robotReducer} from '../../store/reducers/robot.reducer';
import {CommandRunnerService} from '../../services/command-runner/command-runner.service';
import {CommandFactoryService} from '../../services/command-factory/command-factory.service';
import {PositioningService} from '../../services/positioning/positioning.service';
import {TextService} from '../../services/text/text.service';

describe('MainComponent', () => {
  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        StoreModule.forRoot({robot: robotReducer, reportOutput: reportOutputReducer})
      ],
      declarations: [ MainComponent, RobotInputComponent, RobotOutputComponent ],
      providers: [ CommandRunnerService, CommandFactoryService, PositioningService, TextService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
