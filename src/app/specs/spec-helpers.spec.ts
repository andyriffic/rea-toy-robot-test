import {IBoardCoordinate} from '../interfaces/board-coordinate.interface';

export function generateAllValidBoardCoordinate(): IBoardCoordinate[] {
  const allCells: IBoardCoordinate[] = [];
  const maxCellX = 4;
  const maxCellY = 4;

  for (let cellXIndex = 0; cellXIndex <= maxCellX; cellXIndex++) {
    for (let cellYIndex = 0; cellYIndex <= maxCellY; cellYIndex++) {
      const coordinate = <IBoardCoordinate>{x: cellXIndex, y: cellYIndex};
      allCells.push(coordinate);
    }
  }
  return allCells;
}
